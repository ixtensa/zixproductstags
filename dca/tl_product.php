<?php

if (@class_exists("tl_product"))
{

class tl_product_tags extends tl_product
{
	public function deleteProduct($dc)
	{
		$this->Database->prepare("DELETE FROM tl_product_tag WHERE from_table = ? AND tid = ?")
			->execute($dc->table, $dc->id);
	}
	
	public function onCopy($dc)
	{
		if (is_array($this->Session->get('tl_product_copy')))
		{
			foreach ($this->Session->get('tl_product_copy') as $data)
			{
				$this->Database->prepare("INSERT INTO tl_product_tag (tid, tag, from_table) VALUES (?, ?, ?)")
					->execute($dc->id, $data['tag'], $data['table']);
			}
		}
		$this->Session->set('tl_product_copy', null);
		if (\Input::get('act') != 'copy')
		{
			return;
		}
		$objTags = $this->Database->prepare("SELECT * FROM tl_product_tag WHERE tid = ? AND from_table = ?")
			->execute(\Input::get('tid'), $dc->table);
		$tags = array();
		while ($objTags->next())
		{
			array_push($tags, array("table" => $dc->table, "tag" => $objTags->tag));
		}
		$this->Session->set("tl_product_copy", $tags);
	}
}

/**
 * Change tl_product palettes
 */
$disabledObjects = deserialize($GLOBALS['TL_CONFIG']['disabledTagObjects'], true);
if (!in_array('tl_product', $disabledObjects))
{
	$GLOBALS['TL_DCA']['tl_product']['config']['ondelete_callback'][] = array('tl_product_tags', 'deleteProduct');
	$GLOBALS['TL_DCA']['tl_product']['config']['onload_callback'][] = array('tl_product_tags', 'onCopy');
	$GLOBALS['TL_DCA']['tl_product']['palettes']['default'] = str_replace("alias", "alias,tags", $GLOBALS['TL_DCA']['tl_product']['palettes']['default']);
	$GLOBALS['TL_DCA']['tl_product']['palettes']['internal'] = str_replace("alias", "alias,tags", $GLOBALS['TL_DCA']['tl_product']['palettes']['internal']);
	$GLOBALS['TL_DCA']['tl_product']['palettes']['external'] = str_replace("alias", "alias,tags", $GLOBALS['TL_DCA']['tl_product']['palettes']['external']);
}

$GLOBALS['TL_DCA']['tl_product']['fields']['tags'] = array
(
	'label'                   => &$GLOBALS['TL_LANG']['MSC']['tags'],
	'inputType'               => 'tag',
	'eval'                    => array('tl_class'=>'clr long'),
	'sql'                     => "char(1) NOT NULL default ''"
);

}

