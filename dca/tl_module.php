<?php

/**
 * Class tl_module_tags
 *
 * Provide miscellaneous methods that are used by the data configuration array.
 * @copyright  Helmut Schottmüller 2008-2013
 * @author     Helmut Schottmüller <http://www.github.com/hschottm>
 * @package    Controller
 */
class tl_module_tags extends tl_module
{
	/**
	 * Return available tag tables
	 *
	 * @return array Array of tag tables
	 */
	public function getTagTables()
	{
		$objTable = $this->Database->prepare("SELECT DISTINCT(from_table) FROM tl_product_tag ORDER BY from_table")
			->execute();
		$tables = array();
		if ($objTable->numRows)
		{
			while ($objTable->next())
			{
				$tables[$objTable->from_table] = $objTable->from_table;
			}
		}
		return $tables;
	}

	/**
	 * Return all tag cloud templates as array
	 * @param object
	 * @return array
	 */
	public function getTagCloudTemplates(DataContainer $dc)
	{
		return $this->getTemplateGroup('mod_tagcloud', $dc->activeRecord->pid);
	}
}

/**
 * Add palettes to tl_module
 */
$GLOBALS['TL_DCA']['tl_module']['palettes']['tagcloud']    = '{title_legend},name,headline,type;{size_legend},tag_maxtags,tag_buckets,tag_named_class,tag_show_reset;{template_legend:hide},cloud_template;{tagextension_legend},tag_related,tag_topten;{redirect_legend},tag_jumpTo,keep_url_params;{datasource_legend},tag_sourcetables;{expert_legend:hide},tag_tagtable,tag_tagfield,cssID';
$GLOBALS['TL_DCA']['tl_module']['palettes']['productlist']  = str_replace('{template_legend', '{showtags_legend},tag_filter,tag_ignore,product_showtags;{template_legend', $GLOBALS['TL_DCA']['tl_module']['palettes']['productlist']);
$GLOBALS['TL_DCA']['tl_module']['palettes']['__selector__'][] = 'tag_topten';
$GLOBALS['TL_DCA']['tl_module']['palettes']['__selector__'][] = 'product_showtags';
$GLOBALS['TL_DCA']['tl_module']['subpalettes']['tag_topten']    = 'tag_topten_number,tag_topten_expanded,tag_all_expanded';
$GLOBALS['TL_DCA']['tl_module']['subpalettes']['product_showtags']    = 'tag_jumpTo,tag_named_class';

/**
 * Add fields to tl_module
 */

$GLOBALS['TL_DCA']['tl_module']['fields']['tag_sourcetables'] = array
(
	'label'                   => &$GLOBALS['TL_LANG']['tl_module']['tag_forTable'],
	'inputType'               => 'checkbox',
	'options_callback'        => array('tl_module_tags', 'getTagTables'),
	'eval'                    => array('multiple'=>true, 'tl_class' => 'full'),
	'sql'                     => "blob NULL"
);

$GLOBALS['TL_DCA']['tl_module']['fields']['hide_on_empty'] = array
(
	'label'                   => &$GLOBALS['TL_LANG']['tl_module']['hide_on_empty'],
	'inputType'               => 'checkbox',
	'eval'                    => array('multiple'=>false, 'tl_class' => 'w50 m12'),
	'sql'                     => "char(1) NOT NULL default '1'"
);

$GLOBALS['TL_DCA']['tl_module']['fields']['tag_tagtable'] = array
(
	'label'                   => &$GLOBALS['TL_LANG']['tl_module']['tag_tagtable'],
	'default'                 => 'tl_product_tag',
	'inputType'               => 'text',
	'eval'                    => array('maxlength'=>100),
	'sql'                     => "varchar(100) NOT NULL default ''"
);

$GLOBALS['TL_DCA']['tl_module']['fields']['tag_filter'] = array
(
	'label'                   => &$GLOBALS['TL_LANG']['tl_module']['tag_filter'],
	'inputType'               => 'text',
	'eval'                    => array('maxlength'=>255, 'tl_class' => 'w50'),
	'sql'                     => "varchar(255) NOT NULL default ''"
);

$GLOBALS['TL_DCA']['tl_module']['fields']['tag_tagfield'] = array
(
	'label'                   => &$GLOBALS['TL_LANG']['tl_module']['tag_tagfield'],
	'default'                 => 'tag',
	'inputType'               => 'text',
	'eval'                    => array('maxlength'=>100),
	'sql'                     => "varchar(100) NOT NULL default ''"
);

$GLOBALS['TL_DCA']['tl_module']['fields']['tag_maxtags'] = array
(
	'label'                   => &$GLOBALS['TL_LANG']['tl_module']['tag_maxtags'],
	'default'                 => '0',
	'inputType'               => 'text',
	'eval'                    => array('maxlength'=>5, 'rgxp' => 'digit', 'tl_class'=>'w50'),
	'sql'                     => "smallint(5) unsigned NOT NULL default '0'"
);

$GLOBALS['TL_DCA']['tl_module']['fields']['tag_buckets'] = array
(
	'label'                   => &$GLOBALS['TL_LANG']['tl_module']['tag_buckets'],
	'default'                 => '4',
	'inputType'               => 'text',
	'eval'                    => array('maxlength'=>2, 'rgxp' => 'digit', 'tl_class'=>'w50'),
	'sql'                     => "smallint(5) unsigned NOT NULL default '4'"
);

$GLOBALS['TL_DCA']['tl_module']['fields']['tag_named_class'] = array
(
	'label'                   => &$GLOBALS['TL_LANG']['tl_module']['tag_named_class'],
	'inputType'               => 'checkbox',
	'eval'                    => array('tl_class' => 'w50 m12'),
	'sql'                     => "char(1) NOT NULL default ''"
);

$GLOBALS['TL_DCA']['tl_module']['fields']['tag_on_page_class'] = array
(
	'label'                   => &$GLOBALS['TL_LANG']['tl_module']['tag_on_page_class'],
	'inputType'               => 'checkbox',
	'eval'                    => array('tl_class' => 'w50 m12'),
	'sql'                     => "char(1) NOT NULL default ''"
);

$GLOBALS['TL_DCA']['tl_module']['fields']['keep_url_params'] = array
(
	'label'                   => &$GLOBALS['TL_LANG']['tl_module']['keep_url_params'],
	'inputType'               => 'checkbox',
	'eval'                    => array('tl_class' => 'w50 m12'),
	'sql'                     => "char(1) NOT NULL default ''"
);

$GLOBALS['TL_DCA']['tl_module']['fields']['tag_topten'] = array
(
	'label'                   => &$GLOBALS['TL_LANG']['tl_module']['tag_topten'],
	'inputType'               => 'checkbox',
	'eval'                    => array('submitOnChange'=>true),
	'sql'                     => "char(1) NOT NULL default ''"
);

$GLOBALS['TL_DCA']['tl_module']['fields']['tag_topten_number'] = array
(
	'label'                   => &$GLOBALS['TL_LANG']['tl_module']['tag_topten_number'],
	'default'                 => '10',
	'inputType'               => 'text',
	'eval'                    => array('maxlength'=>3, 'rgxp'=> 'digit', 'mandatory' => true, 'tl_class'=>'long'),
	'sql'                     => "varchar(3) NOT NULL default '10'"
);

$GLOBALS['TL_DCA']['tl_module']['fields']['tag_related'] = array
(
	'label'                   => &$GLOBALS['TL_LANG']['tl_module']['tag_related'],
	'inputType'               => 'checkbox',
	'eval'                    => array(),
	'sql'                     => "char(1) NOT NULL default ''"
);

$GLOBALS['TL_DCA']['tl_module']['fields']['tag_topten_expanded'] = array
(
	'label'                   => &$GLOBALS['TL_LANG']['tl_module']['tag_topten_expanded'],
	'inputType'               => 'checkbox',
	'sql'                     => "char(1) NOT NULL default ''"
);

$GLOBALS['TL_DCA']['tl_module']['fields']['tag_all_expanded'] = array
(
	'label'                   => &$GLOBALS['TL_LANG']['tl_module']['tag_all_expanded'],
	'inputType'               => 'checkbox',
	'sql'                     => "char(1) NOT NULL default ''"
);

$GLOBALS['TL_DCA']['tl_module']['fields']['product_showtags'] = array
(
	'label'                   => &$GLOBALS['TL_LANG']['tl_module']['product_showtags'],
	'inputType'               => 'checkbox',
	'eval'                    => array('submitOnChange'=>true, 'class'=>''),
	'sql'                     => "char(1) NOT NULL default ''"
);

$GLOBALS['TL_DCA']['tl_module']['fields']['tag_jumpTo'] = array
(
	'label'                   => &$GLOBALS['TL_LANG']['tl_module']['tag_jumpTo'],
	'inputType'               => 'pageTree',
	'explanation'             => 'jumpTo',
	'eval'                    => array('fieldType'=>'radio', 'helpwizard'=>true, 'class'=>''),
	'sql'                     => "smallint(5) unsigned NOT NULL default '0'"
);

$GLOBALS['TL_DCA']['tl_module']['fields']['tag_ignore'] = array
(
	'label'                   => &$GLOBALS['TL_LANG']['tl_module']['tag_ignore'],
	'inputType'               => 'checkbox',
	'eval'                    => array('tl_class' => 'w50'),
	'sql'                     => "char(1) NOT NULL default ''"
);


$GLOBALS['TL_DCA']['tl_module']['fields']['cloud_template'] = array
(
	'label'                   => &$GLOBALS['TL_LANG']['tl_module']['cloud_template'],
	'default'                 => 'mod_tagcloud',
	'exclude'                 => true,
	'inputType'               => 'select',
	'options_callback'        => array('tl_module_tags', 'getTagCloudTemplates'),
	'eval'                    => array('tl_class'=>'w50'),
	'sql'                     => "varchar(32) NOT NULL default ''"
);

$GLOBALS['TL_DCA']['tl_module']['fields']['tag_show_reset'] = array
(
	'label'                   => &$GLOBALS['TL_LANG']['tl_module']['tag_show_reset'],
	'inputType'               => 'checkbox',
	'eval'                    => array('tl_class' => 'w50 m12'),
	'sql'                     => "char(1) NOT NULL default ''"
);

