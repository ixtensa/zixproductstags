<?php

/**
 * Contao Open Source CMS
 * 
 * Copyright (C) 2005-2012 Leo Feyer
 * 
 * @package tags
 * @link    http://contao.org
 * @license http://www.gnu.org/licenses/lgpl-3.0.html LGPL
 */


/**
 * Run in a custom namespace, so the class can be replaced
 */
namespace IXTENSA;


/**
 * Reads and writes calendars
 * 
 * @package   Models
 * @author    Fabian Perrey
 * @copyright IXTENSA
 */
class TagModel extends \Model
{

	/**
	 * Table name
	 * @var string
	 */
	protected static $strTable = 'tl_product_tag';


	/**
	 * Find multiple tag results by their IDs and source tables
	 * 
	 * @param int $id ID of the element
	 * @param string $table Table name of the element
	 * 
	 * @return \Model\Collection|null A collection of models or null if there are no calendars
	 */
	public static function findByIdAndTable($id, $table)
	{
		return static::findBy(array('tid=?','from_table=?'), array($id, $table), array('order'=>'tag ASC'));
	}

		/**
	 * Find published product items by their parent ID
	 * 
	 * @param array   $arrTags     An array of tag names
	 * @param string  $table       Table name the tags are related to
	 * @param integer $intOffset   An optional ooptions array
	 * 
	 * @return \Model\Collection|null A collection of models or null if there are no matches
	 */
	public static function findByTagsAndTable($arrTags, $table, $arrOptions=array() )
	{
		if (!is_array($arrTags) || empty($arrTags))
		{
			return null;
		}

		$t = static::$strTable;

		$arrColumns = array();
		$arrColumns[] =	"$t.from_table = '$table'";
		$arrColumns[] = "$t.tag IN ('" . implode("', '", $arrTags) . "') GROUP BY tid HAVING COUNT(tid) =".count($arrTags);

		return static::findBy($arrColumns, null);
	}
}
