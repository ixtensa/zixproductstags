<?php

/**
 * @copyright  IXTENSA
 * @author     Fabian Perrey
 * @package    zixProducts
 * @license    LGPL
 */

namespace IXTENSA;

if (!defined('TL_ROOT')) die('You can not access this file directly!');

/**
 * Class ModuleProductDetailsTags
 *
 * Front end module "product details".
 * @copyright  Helmut Schottmüller 2009
 * @author     Helmut Schottmüller <typolight@aurealis.de>
 * @package    Controller
 */
class ModuleProductDetailsTags extends \ModuleProductDetails
{
	/**
	 * Parse one or more items and return them as array
	 * @param object
	 * @param boolean
	 * @return array
	 */
	protected function compile()
	{
		$this->Session->set('product_showtags', $this->product_showtags);
		$this->Session->set('product_jumpto', $this->tag_jumpTo);
		$this->Session->set('product_tag_named_class', $this->tag_named_class);
		parent::compile();
		$this->Session->set('product_showtags', '');
		$this->Session->set('product_jumpto', '');
		$this->Session->set('product_tag_named_class', '');
	}
}

?>