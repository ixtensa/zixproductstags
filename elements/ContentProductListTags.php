<?php

/**
 * @copyright  Helmut Schottmüller 2009
 * @author     Helmut Schottmüller <typolight@aurealis.de>
 * @package    zixProduct
 * @license    LGPL
 * @filesource
 */

namespace IXTENSA;

if (!defined('TL_ROOT')) die('You can not access this file directly!');

/**
 * Class ContentProductListTags
 *
 * Content element "product list".
 * @copyright  Helmut Schottmüller 2009
 * @author     Helmut Schottmüller <typolight@aurealis.de>
 * @package    Controller
 */
class ContentProductListTags extends \ContentProductList
{
	/**
	 * Display a wildcard in the back end
	 * @return string
	 */
	public function generate()
	{
		return parent::generate();
	}

	/**
	 * Read tags from database
	 * @return string
	 */
	protected function getFilterTags()
	{
		if (strlen($this->tag_filter))
		{
			$tags = preg_split("/,/", $this->tag_filter);
			$placeholders = array();
			foreach ($tags as $tag)
			{
				array_push($placeholders, '?');
			}
			array_push($tags, 'tl_product');
			return $this->Database->prepare("SELECT tid FROM tl_product_tag WHERE tag IN (" . join($placeholders, ',') . ") AND from_table = ? ORDER BY tag ASC")
				->execute($tags)
				->fetchEach('tid');
		}
		else
		{
			return array();
		}
	}

	/**
	 * Generate the module
	 */
	protected function compileFromParent($arrIds)
	{
		$offset = intval($this->skipFirst);
		$limit = null;

		// Maximum number of items
		if ($this->numberOfItems > 0)
		{
			$limit = $this->numberOfItems;
		}

		$this->Template->articles = array();
		$this->Template->empty = $GLOBALS['TL_LANG']['MSC']['emptyTagList'];

		// Get the total number of items
		$intTotal = 0;
		if (count($arrIds) > 0)
		{
			$intTotal = \ProductTagModel::countPublishedByPidsAndIds($this->product_archives, $arrIds);
		}

		if ($intTotal < 1)
		{
			return;
		}

		$total = $intTotal - $offset;

		// Split the results
		if ($this->perPage > 0 && (!isset($limit) || $this->numberOfItems > $this->perPage))
		{
			// Adjust the overall limit
			if (isset($limit))
			{
				$total = min($limit, $total);
			}

			// Get the current page
			$id = 'page_n' . $this->id;
			$page = \Input::get($id) ?: 1;

			// Do not index or cache the page if the page number is outside the range
			if ($page < 1 || $page > max(ceil($total/$this->perPage), 1))
			{
				global $objPage;
				$objPage->noSearch = 1;
				$objPage->cache = 0;

				// Send a 404 header
				header('HTTP/1.1 404 Not Found');
				return;
			}

			// Set limit and offset
			$limit = $this->perPage;
			$offset += (max($page, 1) - 1) * $this->perPage;
			$skip = intval($this->skipFirst);

			// Overall limit
			if ($offset + $limit > $total + $skip)
			{
				$limit = $total + $skip - $offset;
			}

			// Add the pagination menu
			$objPagination = new \Pagination($total, $this->perPage, \Config::get('maxPaginationLinks'), $id);
			$this->Template->pagination = $objPagination->generate("\n  ");
		}

		// Get the items
		if (isset($limit))
		{
			$objArticles = \ProductTagModel::findPublishedByPidsAndIds($this->product_archives, $arrIds, $limit, $offset);
		}
		else
		{
			$objArticles = \ProductTagModel::findPublishedByPidsAndIds($this->product_archives, $arrIds, 0, $offset);
		}

		// Add the articles
		if ($objArticles !== null)
		{
			$this->Template->articles = $this->parseArticles($objArticles);
		}

		$this->Template->archives = $this->product_archives;
		
		// new code for tags
		$relatedlist = (strlen(\Input::get('related'))) ? preg_split("/,/", \Input::get('related')) : array();
		$headlinetags = array();
		if (strlen(\Input::get('tag')))
		{
			$headlinetags = array_merge($headlinetags, array(\Input::get('tag')));
			if (count($relatedlist))
			{
				$headlinetags = array_merge($headlinetags, $relatedlist);
			}
		}
		$this->Template->tags_total_found = $intTotal;
		$this->Template->tags_activetags = $headlinetags;
	}

	/**
	 * Generate module
	 */
	protected function compile()
	{
		$this->Session->set('product_showtags', $this->product_showtags);
		$this->Session->set('product_jumpto', $this->tag_jumpTo);
		$this->Session->set('product_tag_named_class', $this->tag_named_class);
		if ((strlen(\Input::get('tag')) && (!$this->tag_ignore)) || (strlen($this->tag_filter)))
		{					
			$relatedlist = (strlen(\Input::get('related'))) ? preg_split("/,/", \Input::get('related')) : array();
			$alltags = array_merge(preg_split("/,/", \Input::get('tag')), $relatedlist);
			
			$first = true;
			if (strlen($this->tag_filter))
			{
				$headlinetags = preg_split("/,/", $this->tag_filter);
				$tagids = $this->getFilterTags();
				$first = false;
			}
			else
			{
				$headlinetags = array();
			}

			$objTags = \TagModel::findByTagsAndTable($alltags, 'tl_product');

			$arrTagIds = array();
			if ($objTags != null)
			{
				while ( $objTags->next() )
				{
					$arrTagIds[] = $objTags->tid;
				}

			}

			$this->compileFromParent($arrTagIds);
		}
		else
		{
			parent::compile();
		}
		$this->Session->set('product_showtags', '');
		$this->Session->set('product_jumpto', '');
		$this->Session->set('product_tag_named_class', '');
	}
}

?>