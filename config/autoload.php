<?php

/**
 * Contao Open Source CMS
 *
 * Copyright (c) 2005-2014 Leo Feyer
 *
 * @package zixProductsTags
 * @link    https://contao.org
 * @license http://www.gnu.org/licenses/lgpl-3.0.html LGPL
 */


/**
 * Register namespaces
 */
ClassLoader::addNamespaces(array
(
	'IXTENSA'
));


/**
 * Register the classes
 */
ClassLoader::addClasses(array
(
	// Classes
	'IXTENSA\ProductTag'                => 'system/modules/zixProductsTags/classes/ProductTag.php',
	'IXTENSA\TagField'                  => 'system/modules/zixProductsTags/classes/TagField.php',
	'IXTENSA\TagHelper'                 => 'system/modules/zixProductsTags/classes/TagHelper.php',
	'IXTENSA\TagList'                   => 'system/modules/zixProductsTags/classes/TagList.php',

	// Elements
	'IXTENSA\ContentProductListTags'    => 'system/modules/zixProductsTags/elements/ContentProductListTags.php',
	'IXTENSA\ContentProductDetailsTags' => 'system/modules/zixProductsTags/elements/ContentProductDetailsTags.php',

	// Models
	'IXTENSA\TagModel'                  => 'system/modules/zixProductsTags/models/TagModel.php',
	'IXTENSA\ProductTagModel'           => 'system/modules/zixProductsTags/models/ProductTagModel.php',

	// Modules
	'IXTENSA\ModuleProductListTags'     => 'system/modules/zixProductsTags/modules/ModuleProductListTags.php',
	'IXTENSA\ModuleProductDetailsTags'  => 'system/modules/zixProductsTags/modules/ModuleProductDetailsTags.php',
	'IXTENSA\ModuleTagCloud'            => 'system/modules/zixProductsTags/modules/ModuleTagCloud.php',
));


/**
 * Register the templates
 */
TemplateLoader::addFiles(array
(
	'mod_tagcloud'             => 'system/modules/zixProductsTags/templates/modules',
	'product_full_tags'        => 'system/modules/zixProductsTags/templates/product',

));
