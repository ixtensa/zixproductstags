<?php if (!defined('TL_ROOT')) die('You can not access this file directly!');

/**
 * TYPOlight webCMS
 * Copyright (C) 2005 Leo Feyer
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation, either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program. If not, please visit the Free
 * Software Foundation website at http://www.gnu.org/licenses/.
 *
 * PHP version 5
 * @copyright  IXTENSA
 * @author     Fabian Perrey
 * @package    Backend
 * @license    LGPL
 */


/**
 * Form fields
 */
$GLOBALS['BE_FFL']['tag'] = 'TagField';

/**
 * Front end modules
 */
array_insert($GLOBALS['FE_MOD']['tags'], 1, array
(
	'tagcloud'    => 'ModuleTagCloud'
));

$GLOBALS['FE_MOD']['product']['productlist'] = 'ModuleProductListTags';
$GLOBALS['FE_MOD']['product']['productdetails'] = 'ModuleProductDetailsTags';


/**
 * Content elements
 */
$GLOBALS['TL_CTE']['product']['productlist'] = 'ContentProductListTags';
$GLOBALS['TL_CTE']['product']['productdetails'] = 'ContentProductDetailsTags';
	
if (TL_MODE == 'BE')
{
	/**
	 * CSS files
	 */

	if (is_array($GLOBALS['TL_CSS']))
	{
		array_insert($GLOBALS['TL_CSS'], 1, 'system/modules/zixProductsTags/assets/tag.css');
	}
	else
	{
		$GLOBALS['TL_CSS'] = array('system/modules/zixProductsTags/assets/tag.css');
	}

	/**
	 * JavaScript files
	 */
	if (is_array($GLOBALS['TL_JAVASCRIPT']))
	{
		array_insert($GLOBALS['TL_JAVASCRIPT'], 1, 'system/modules/zixProductsTags/assets/tag.js');
	}
	else
	{
		$GLOBALS['TL_JAVASCRIPT'] = array('system/modules/zixProductsTags/assets/tag.js');
	}
}

/**
 * Hooks
 */
$GLOBALS['TL_HOOKS']['reviseTable'][] = array('TagHelper', 'deleteIncompleteRecords');
$GLOBALS['TL_HOOKS']['reviseTable'][] = array('TagHelper', 'deleteUnusedTagsForTable');
$GLOBALS['TL_HOOKS']['replaceInsertTags'][] = array('TagHelper', 'replaceTagInsertTags');
$GLOBALS['TL_HOOKS']['parseArticles'][] = array('TagHelper', 'parseArticlesHook');

/**
* source tables that have tags enabled
*/
$GLOBALS['tags_extension']['sourcetable'][] = 'tl_product';

/**
* Add 'tag' to the URL keywords to prevent problems with URL manipulating modules like folderurl
*/
$GLOBALS['TL_CONFIG']['urlKeywords'] .= (strlen(trim($GLOBALS['TL_CONFIG']['urlKeywords'])) ? ',' : '') . 'tag';

?>
